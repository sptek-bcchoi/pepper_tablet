
$(function() {
    var previewRunning = false;
    var context;
    var imgData;
    var counter=0;
    $('#idle_btn_01').on({
        'touchstart': function(){
            location.href='page01.html';
        }
    })
    $('#page_btn_01').on({
        'touchstart': function(){
            location.href='index.html';
        }
    })
    $('.contents-item-1').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "contents-item-1");
        }
    })
    $('.contents-item-2').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "contents-item-2");
        }
    })
    $('.contents-item-3').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "contents-item-3");
        }
    })
    $('.contents-item-4').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "contents-item-4");
        }
    })
    $('.contents-item-6').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "contents-item-6");
        }
    })
    $('.test_button').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "test_button");
        }
    })
    $('.goods-start-button-link').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "goods-start-button-link");
        }
    })
    $('.w-button').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "w-button");
        }
    })
    $('.home_button').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "home_button");
        }
    })
    $('.m1_1_goods').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "m1_1_goods");
        }
    })
    $('.m1_2_goods').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "m1_2_goods");
        }
    })
    $('.m1_3_cards').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "m1_3_cards");
        }
    })
    $('.m1_back').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "m1_back");
        }
    })
    $('.m4_back').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "m4_back");
        }
    })
    $('.main-guide-btn').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "main-guide-btn");
        }
    })
    $('.m4_1').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "m4_1");
        }
    })
    $('.m4_2').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "m4_2");
        }
    })
    $('.m4_3').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "m4_3");
        }
    })
    $('.beer_detect_guide').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "beer_detect_guide");
        }
    })
    $('.beer_main').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "beer_main");
        }
    })
    $('.beer_scan').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "beer_scan");
        }
    })
    $('.beer_guide').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "beer_guide");
        }
    })
    $('.banner-item-1').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "banner-item-1");
        }
    })
    $('.banner-item-2').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "banner-item-2");
        }
    })
    $('.banner-item-3').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "banner-item-3");
        }
    })
    $('.banner-item-4').on({
        'touchstart': function(){
  	        $.raiseALMemoryEvent("by_web/events", "banner-item-4");
  	    }
  	})
    $('.m4_pose_1').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "m4_pose_1");
        }
    })
    $('.m4_pose_2').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "m4_pose_2");
        }
    })
    $('.m4_pose_3').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "m4_pose_3");
        }
    })
    $('.m4_pose_4').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "m4_pose_4");
        }
    })
    $('.recobeer').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "recobeer");
        }
    })
    $('.recosite').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "recoside");
        }
    })
    $('.beer_result').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "beer_result");
        }
    })
    $('.m4_face_good').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "m4_face_good");
        }
    })
    $('.m4_face_bad').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "m4_face_bad");
        }
    })
    $('.card_view_back').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "card_view_back");
        }
    })
    $('.nh_btn').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "nh_btn");
        }
    })
    $('.lotte_btn').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "lotte_btn");
        }
    })
    $('.woori_btn').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "woori_btn");
        }
    })
    $('.bc_btn').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "bc_btn");
        }
    })
    $('.hyundai_btn').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "hyundai_btn");
        }
    })
    $('.shinhan_btn').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "shinhan_btn");
        }
    })
    $('.samsung_btn').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "samsung_btn");
        }
    })
    $('.citi_btn').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "citi_btn");
        }
    })
    $('.kb_btn').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "kb_btn");
        }
    })
    $('.hana_btn').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "hana_btn");
        }
    })
    $('.hyundaie_btn').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "hyundaie_btn");
        }
    })
    $('.ibk_btn').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "ibk_btn");
        }
    })
    $('.sc_btn').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "sc_btn");
        }
    })
    $('.ssg_btn').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "ssg_btn");
        }
    })
    $('.main_guide').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "main_guide");
        }
    })
    $('.q_closeddates').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "q_closeddates");
        }
    })
    $('.q_delivery').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "q_delivery");
        }
    })
    $('.q_receipt').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "q_receipt");
        }
    })
    $('.q_refund').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "q_refund");
        }
    })
    $('.m3_close').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "m3_close");
        }
    })
    $('.q_customercenter').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "q_customercenter");
        }
    })
    $('.q_locker').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "q_locker");
        }
    })
    $('.q_toilet').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "q_toilet");
        }
    })
    $('.q_babyroom').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "q_babyroom");
        }
    })
    $('.q_culturecenter').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "q_culturecenter");
        }
    })
    $('.m0_day').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "m0_day");
        }
    })
    $('.m0_night').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "m0_night");
        }
    })
    
    /*poc2*/
    $('.m0_drive').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "m0_drive");
        }
    })
    $('.m0_foods').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "m0_foods");
        }
    })
    $('.m0_country').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "m0_country");
        }
    })
    $('.m0_cook').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "m0_cook");
        }
    })
     $('.sauce_home_btn').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "sauce_home_btn");
        }
    })
     $('.sauce_back_btn').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "sauce_back_btn");
        }
    })
    $('.a30010').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "30010");
        }
    })
    $('.a30020').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "30020");
        }
    })
    $('.a30030').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "30030");
        }
    })
    $('.a30040').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "30040");
        }
    })
    $('.a30050').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "30050");
        }
    })
    $('.a30060').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "30060");
        }
    })
    $('.sauceList01').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "1");
        }
    })
    $('.sauceList02').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "2");
        }
    })
    $('.sauceList03').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "3");
        }
    })
    $('.btnLoc').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "btn_loc");
        }
    })
    $('.btn_przone').on({
    	 'touchstart': function(){
             $.raiseALMemoryEvent("by_web/events", "btn_przone");
       }
   })
       $('.btn_przone_end').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "btn_przone_end");
        }
    })
   $('.a31010').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "31010");
        }
    })
    $('.a31020').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "31020");
        }
    })
    $('.a31030').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "31030");
        }
    })
    $('.a31040').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "31040");
        }
    })
    $('.a31050').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "31050");
        }
    })
    $('.a31060').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "31060");
        }
    })
    $('.a31070').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "31070");
        }
    })
    $('.a31080').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "31080");
        }
    })
    $('.a31090').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "31090");
        }
    })
    $('.loc1').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "loc1");
        }
    })
    $('.loc2').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "loc2");
        }
    })
    $('.loc3').on({
        'touchstart': function(){
              $.raiseALMemoryEvent("by_web/events", "loc3");
        }
    })

    
    $.subscribeToALMemoryEvent("by_smach/events", function(data){
        var res = data.split("?");
        if(res.length == 1){
            var fileExt = res[0].substring(res[0].lastIndexOf('.')+1);
            if(fileExt.toUpperCase() == 'HTML'){
                location.href = res[0];
            }

        }else{
            location.href = data;
        }

    })
    $.subscribeToALMemoryEvent("by_xar/events/preview", function(data){
        // preview

        Preview(data);

    })
    function Preview(subscriberId){
        $.getService('ALVideoDevice', function(VideoDevice){

            if(subscriberId.length > 0) {
                getImage(VideoDevice, subscriberId);
                previewRunning = true;
            }
            else{
                previewRunning = false;
            }
        })
    }

    function getImage(VideoDevice, subscriberId){
        $('#subtitle').text(counter++);
        VideoDevice.getImageRemote(subscriberId).then(function (image) {
            if(image){
                var imageWidth = image[0];
                var imageHeight = image[1];
                var imageBuf = image[6];
                if (!context) {
                    context = document.getElementById("canvas").getContext("2d");
                    canvas = document.getElementById("canvas");
                    context.translate(canvas.width/2, canvas.height/2)
                    context.save();
                    context.scale(-1, 1)
                }
                if (!imgData || imageWidth != imgData.width || imageHeight != imgData.height) {
                    imgData = context.createImageData(imageWidth, imageHeight);
                }
                var data = imgData.data;
                for (var i = 0, len = imageHeight * imageWidth; i < len; i++) {
                    var v = imageBuf[i];
                    data[i * 4 + (0)] = v;
                    data[i * 4 + (1)] = v;
                    data[i * 4 + (2)] = v;
                    data[i * 4 + (3)] = 255;
                }
                context.putImageData(imgData, 0, 0);
            }
            if(previewRunning) {
                setTimeout(function() { getImage(VideoDevice, subscriberId) }, 100)
            }
        })
    }
});
