var Constants = (function() {
    return {
        API_SERVER_URL: "http://211.117.60.137",
        API_SERVICE_QNA: "/service/questionAnswer.do",
        API_SERVICE_BEER_INFO: "/service/beerInfo.do",
        API_SERVICE_RECO_BEER_LIST: "/service/recoBeerList.do",
        API_SERVICE_RECO_SIDE_LIST: "/service/recoSideList.do",
        API_SERVICE_DAYOFF_INFO: "/service/dayOffInfo.do",
        API_SERVICE_CARD_INFO: "/service/cardInfo.do",
        API_SERVICE_CARD_EVENT_INFO: "/service/cardEventInfo.do",
        API_SERVICE_LEAFLET_INFO: "/service/leafletInfo.do",
        API_SERVICE_LEAFLET_GOODS_INFO: "/service/leafletGoodsInfo.do",
		API_SERVICE_SAUCE_LIST: "/service/sauceList.do",
		API_SERVICE_SAUCE_INFO: "/service/sauceInfo.do",
		API_SERVICE_COOK_CATEGORY_INFO: "/service/cookCategoryInfo.do",
        API_TABLET: "/tablet/",
        API_LOGGING: "/loging/",   
        IMAGE_SERVER_URL: "http://211.117.60.137:1080/",
        TEMP :"000000000000",
        CUSTROMER_MENU_BEST:"11000",  //BEST5
        CUSTROMER_MENU_EXCHANGE:"11001", // 교환/환불/AS
        CUSTROMER_MENU_STORE:"11002", // 매장 이용
        CUSTROMER_MENU_LOCATION:"11003", // 주요시설 위치
        CUSTROMER_MENU_GIFT:"11004", // 상품권
        CUSTROMER_MENU_PAYMENT:"11005", // 결제 
        CUSTROMER_MENU_ETC:"11009", // 기타
 
    }
})();

var value = {
		param: 'aaa',
	    // 메소드
	    set: function(food) {
	    	param = food
	        alert(param);
	    }
	};
function cool_function(event){ alert(event.data.param1); alert(event.data.param2); };
var productList = [                 
                       ['차돌백이 된장찌개', '4,800' ,  ' 오늘 저녁은 직접 우려낸 한우 사골육수에<br/>야들야들한 차돌박이를 듬뿍 넣어 더<br/>구수하고 진한 된장찌개 어떠세요?',  'peacock1.png'],
                       ['피콕반점 짬뽕', '6,980',  '살아있는 식감 그대로 얼린 냉동이라<br/>야채와 해물 등이 생생하게 살아있는<br/>건더기의 맛을 느껴보세요!!',  'peacock2.png'],
                       ['마르게리타 피자', '6,980',  '24시간 숙성시킨 도우를 손으로<br/>직접 만들어 중심부분은 얇고 사이드는<br/>두툼한 수제피자를 고객님께 추천합니다!',  'peacock3.png']                       
               ];

var qnaList = [
               ['closeddates', '휴점일 및 영업시간', '  * 영업시간 :  오전 10시부터 23시까지 운영(일부 매장은 다름)<br /><br /> * 휴점일 :   월 2회 휴업(일부 매장은 다름)<br />'],
               ['refund','교환환불 방법 및 기간','   * 반드시 영수증 지참 및 구입 후 한달 이내 (신선, 냉동 식품은  7일 이내)<br /> * 영수증 분실 시, 결제한 신용카드 및 포인트 카드로 구매 내역 확인 후 가능<br /> * 농수축산, 즉석조리, 냉장상품은 판매 가능한 정상상품에 한해 구입가격으로 환불 가능<br /> * 카드 거래 취소 시는 결제한 카드를 지참 하고 방문 시 취소 가능<br /> * 결제 수단 변경 또는 환불을 위해서는 결제한 카드와 재결제 수단을 지참 후 방문 필요<br /> * 패키지 개봉/파손, 의류수선 시 불가. <br /> * 증정품/경품이 있는 경우 반납 후 가능 구입점포에서 환불 가능'],
               ['delivery','매장 구입 상품 배달','   * 대형가전(TV는 29인치 이상) / 대형가구 등의 제한적인 품목에  <br />&nbsp;&nbsp; 한하여 배송 <br />* 성수점 근거리 배송<br /> &nbsp; &nbsp;• 배송 가능 지역 : 성수동 1/2가<br /> &nbsp; &nbsp;• 3만원 이상 구매 시, 무료 배달<br /> &nbsp; &nbsp;• 접수 장소 : 1층 고객만족센터<br /> &nbsp;&nbsp; • 근거리 배송은 하루 40건으로 한정'],
               ['receipt','영수증 분실 시 교환 및 환불',' * 구매한지 1개월 이내는 결제하신 카드 또는 이마트 포인트 카드로 구매 내역<br />    &nbsp;&nbsp;  확인 후  교환/환불 가능<br /> * 교환 환불시 이마트 포인트 내역을 고객센터에서 확인 가능<br /> * 구입하신 점포에서만 가능합니다']
];

var cardList = [
			['BC card','bccard','13001'],
			['CITY CARD','citicard','13002'],
			['하나카드','hanacard','13003'],
			['Hyundai Card','hyundalcard','13004'],
			['KB국민카드','kookmincard','13005'],
			['LOTTE CARD','lottecard','13006'],
			['NH농협카드','nonghyubcard','13007'],
			['SAMSUNG CARD','samsungcard','13008'],
			['ShinhanCard','shinhancard','13009'],
			['우리카드','wooricard','13010'],
			['SSG PAY','ssgpay','13011'],
			['Hyundai Emart Card','emtcard','13012'],
			['IBK 기업은행','ibkcard','13013'],
			['스탠다드차타드 은행','sccard','13014']
];


var locationList = [
	  		 ['customercenter_day','고객센터','cut_map_f1_3_ent.png'],
			 ['locker_day','물품보관함','cut_map_f1_2_ent.png'],
			 ['toilet_day','화장실(1층)','cut_map_f1_1_ent.png'],
			 ['babyroom_day','유아휴게실','cut_map_f2.png'],
			 ['culturecenter_day','문화센터','cut_map_f3.png'],
			 ['customercenter_night','고객센터','cut_map_f1_3.png'],
			 ['locker_night','물품보관함','cut_map_f1_2.png'],
			 ['toilet_night','화장실(1층)','cut_map_f1_1.png'],
			 ['babyroom_night','유아휴게실','cut_map_f2.png'],
			 ['culturecenter_night','문화센터','cut_map_f3.png']
       ];

var recoLocationList = [
			['poi_1','과자','bu_loc c_blue','772','406','790','214','map_line.png'],
			['poi_2','견과류/마른안주','bu_loc c_purple','739','458','738','215','map_line5.png'],
			['poi_3','마른오징어','bu_loc c_green','900','502','798','210','map_line4.png'],
			['poi_4','육포','bu_loc c_orange','985','502','800','210','map_line3.png'],
			['poi_5','치킨/튀김','bu_loc c_red','1085','235','800','142','map_line2.png'],
			['poi_6','치킨/튀김','bu_loc c_red','1085','235','800','142','map_line2.png']
];

var foodIntents = [
			  ['POC2_food_category1','우동, 소바용 국물 쯔유와 <br/>소바면, 와사비를 알려드릴게요.'],
			  ['POC2_food_category2','스키야키용 국물 소스와 <br/>찍어먹는 소스를 알려드릴게요.'],
			  ['POC2_food_category3','덮밥류를 만들 수 있는 카레와 후리카게를 알려드릴게요.'],
			  ['POC2_food_category4','월남쌈을 찍어먹는 소스와 라이스페이퍼를 알려드릴게요.'],
			  ['POC2_food_category5','간편 쌀국수와 쌀국수 국물, 볶음용 소스를 알려드릴게요.'],
			  ['POC2_food_category6','해산물, 볶음용 굴, 칠리 소스와 볶음밥 소스를 알려드릴게요.'],
			  ['POC2_food_category7','튀김, 볶음용 찍어먹는 소스와 볶음 소스를 알려드릴게요.'],
			  ['POC2_food_category8','생선회에 사용되는 쇼유와 와사비 소스를 알려드릴게요.'],
			  ['POC2_food_category9','일본식과 아시안식의 인스턴트 라면들을 알려드릴게요.']
];

//중앙 point의 분산형 배열
var abvValue = [50.5, 51, 51.5, 52, 52.5, 53, 53.5, 54, 54.5, 55, 55.5, 56, 56.5, 57, 57.5, 58, 58.5, 59, 59.5, 60, 
                64, 68, 72, 76, 80, 84, 88, 92, 96, 100, 106, 112, 118, 124, 130, 136, 142, 148, 154, 160, 171, 182, 193, 
                204, 215, 226, 237, 248, 259, 270, 281, 292, 303, 314, 325, 336, 347, 358, 369, 380, 386, 392, 398, 
                404, 410, 416, 422, 428, 434, 440, 444, 448, 452, 456, 460, 464, 468, 472, 476, 480, 481, 482, 483, 484, 
                485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500];

//var ibuValue = [290, 270, 243, 216, 179, 152, 124, 97, 70, 42, 15];

var ibuValue = [289.23, 286.46, 283.69, 280.92, 278.15, 275.38, 272.61, 269.84, 267.07, 264.3, 261.53, 258.76, 
                255.99, 253.22, 250.45, 247.68, 244.91, 242.14, 239.37, 236.6, 233.83, 231.06, 228.29, 225.52, 222.75, 219.98,
                217.21, 214.44, 211.67, 208.9, 206.13, 203.36, 200.59, 197.82, 195.05, 192.28, 189.51, 186.74, 183.97, 181.2, 178.43,
                175.66, 172.89, 170.12, 167.35, 164.58, 161.81, 159.04, 156.27, 153.5, 150.73, 147.96, 145.19, 142.42, 139.65, 136.88, 
                134.11, 131.34, 128.57, 125.8, 123.03, 120.26, 117.49, 114.72, 111.95, 109.18, 106.41, 103.64, 100.87, 98.1, 95.33, 92.56, 
                89.79, 87.02, 84.25, 81.48, 78.71, 75.94, 73.17, 70.4, 67.63, 64.86, 62.09, 59.32, 56.55, 53.78, 51.01, 48.24, 45.47, 42.7, 
                39.93, 37.16, 34.39, 31.62, 28.85, 26.08, 23.31, 20.54, 17.77, 15];

var rankValue = [
                 '☆',
                 '★',
                 '★☆',
                 '★★',
                 '★★☆',
                 '★★★',
                 '★★★☆',
                 '★★★★',
                 '★★★★☆',
                 '★★★★★'
];

function rankValueRange(value){
	var intStr = 0;
	var str = Number(value)*10;
    if( str > 0    &&   str  <= 5){
		 intStr =0;
    }else if(   str > 5  &&  str <= 10){
		intStr =1;
	}else if( str > 10  &&   str <= 15){
			intStr =2;
	}else if( str > 15  &&   str <= 20){
			intStr =3;
	}else if( str > 20  &&  str <= 25){
			intStr =4;
	}else if( str > 25  &&   str <= 30){
			intStr =5;
	}else if( str > 30  &&   str  <=35){
			intStr =6;
	}else if( str > 35  &&   str <= 40){
			intStr =7;
	}else if( str > 40  &&   str <= 45){
			intStr =8;
	}else if( str > 45  &&   str <= 50){
			intStr =9;
	}
     return rankValue[intStr];
}

function param_function(event){
	 alert(event.data.param1); 
 }

function abvValueRange(str){
	var intStr = 0;
	
	if( Number(str) > 9.9 ){
		 intStr =  99;
	}else {
	     intStr =  Number(str)*10 - 1;
	}
     return abvValue[intStr];
}

function ibuValueRange(str){
	 var intStr = 0;

	 if(Number(str) == 0){
			str = 1;
	} else if(Number(str) > 99){
		   str = 100;
	}
	intStr =Math.round(Number(str))-1;
  
	 return ibuValue[intStr];
}

/*function ibuValueRange(str){
	 var intStr = 0;
	if (str.length > 1){
		intStr =Math.round(Number(str)/10);
	}else {
        //if(Number(str) == 0){
			str = 1;
	//}
	    intStr = Number(str) - 1;
    }
	 return ibuValue[intStr];
	}*/

function sendLocationQuery(url) {
    var xhttp = new XMLHttpRequest();
    
    xhttp.open("GET", url, false);
    xhttp.send('id=20');
};

function  setTempstr (str){
	
	this.tempStr = str ;
}

var StringBuffer = function() {
    this.buffer = new Array();
}

StringBuffer.prototype.append = function(str) {
    this.buffer[this.buffer.length] = str;
}

StringBuffer.prototype.toString = function() {
    return this.buffer.join("");
}

function ConvertSystemSourcetoHtml(str){
	 str = str.replace(/</g,"&lt;");
	 str = str.replace(/>/g,"&gt;");
	 str = str.replace(/\"/g,"&quot;");
	 str = str.replace(/\'/g,"");
	 str = str.replace(/\n/g,"<br />");
	 str = str.replace(/& #40;/g, '(');
	 str = str.replace(/& #41;/g, ')');
	 str = str .replace(/& gt;/g, '>');
	 return str;
	}

var spCharCub = function(str) {
	var char = str;
	
	if(char != "" && char != null) {
		char = char.replace(/& amp;/g, "&");
		char = char.replace(/& #35;/g, '#');
		char = char.replace(/& #59;/g, '"');
		char = char.replace(/& lt;/g, '<');
		char = char.replace(/& #92;/g, '\\\\');
		char = char.replace(/& gt;/g, '>');
		char = char.replace(/& #40;/g, '(');
		char = char.replace(/& #41;/g, ')');
		char = char.replace(/& #39;/g, "'");
		char = char.replace(/& quot;/g, "\\");
		char = char.replace(/& #36;/g, '\\$');
		char = char.replace(/& #42;/g, '*');
		char = char.replace(/& #43;/g, '+');
		char = char.replace(/& #124;/g, '|');
		char = char.replace(/& #46;/g, '\\.');
		char = char.replace(/& #63;/g, '\\?');
		char = char.replace(/& #91;/g, '\\[');
		char = char.replace(/& #93;/g, '\\]');
		char = char.replace(/& #94;/g, '\\^');
		char = char.replace(/& #123;/g, '\\{');
		char = char.replace(/& #125;/g, '\\}');
		char = char.replace(/& #33;/g, '!');
		char = char.replace(/& #37;/g, '%');
		char = char.replace(/& #44;/g, ',');
		char = char.replace(/& #45;/g, '-');
		char = char.replace(/& #47;/g, '/');
		char = char.replace(/& #58;/g, ':');
		char = char.replace(/& #61;/g, '=');
		char = char.replace(/& #64;/g, '@');
		char = char.replace(/& #95;/g, '_');
		char = char.replace(/& #96;/g, '`');
		char = char.replace(/& #126;/g, '~');
	}
	
	return char;
}

function tranNow() {
	var date = new Date();
	var m = date.getMonth()+1;
	var d = date.getDate();
	var h = date.getHours();
	var i = date.getMinutes();
	var s = date.getSeconds();
	return date.getFullYear()+''+(m>9?m:'0'+m)+''+(d>9?d:'0'+d)+''+(h>9?h:'0'+h)+''+(i>9?i:'0'+i)+''+(s>9?s:'0'+s);
}

var generateRandom = function (min, max) {
	  var ranNum = Math.floor(Math.random()*(max-min+1)) + min;
	  return ranNum;
	}
 	
function addComma(num) {
	  var regexp = /\B(?=(\d{3})+(?!\d))/g;
	   return num.toString().replace(regexp, ',');
	}
